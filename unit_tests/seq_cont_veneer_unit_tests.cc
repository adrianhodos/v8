#include <gtest/gtest.h>
#include <v8/v8.hpp>
#include <v8/base/count_of.hpp>
#include <v8/base/sequence_container_veneer.hpp>

namespace {

struct null_destruct_fn {
    void operator()(int) const {
    }
};

typedef v8::base::sequence_container_veneer<std::vector<int>, null_destruct_fn>
    int_vec_veneer_t;

int_vec_veneer_t make_tmp_veneer(v8_size_t count) {
    return int_vec_veneer_t(count);
}

}

TEST(seq_cont_veneer_tests, constructors) {
    //
    // Default constructor
    int_vec_veneer_t v0;
    EXPECT_EQ(v8_size_t(0), v0.size());

    //
    // Construct with count
    int_vec_veneer_t v1(v8_size_t(10));
    EXPECT_EQ(v8_size_t(10), v1.size());

    //
    // Construct with count and specified value
    int_vec_veneer_t v5(2U, -1);
    EXPECT_EQ(-1, v5[0]);
    EXPECT_EQ(-1, v5[1]);

    v0 = v1;

#if defined(V8_COMPILER_HAS_CXX11_MOVE_SEMANTICS)
    int_vec_veneer_t v2(make_tmp_veneer(10U));
    EXPECT_EQ(10U, v2.size());

    v2 = make_tmp_veneer(4U);
    EXPECT_EQ(4U, v2.size());
#endif    

#if defined(V8_COMPILER_HAS_CXX11_INITIALIZER_LISTS)
    int_vec_veneer_t v3 { 0, 1, 2, 3, 4 };
    EXPECT_EQ(5U, v3.size());

    v3 = { 1, 2 };
    EXPECT_EQ(2U, v3.size());
#endif    

    int arr[] = { 0, 1, 2, 3, 4, 5 };

    int_vec_veneer_t v4(&arr[0], &arr[0] + dimension_of(arr));
    EXPECT_EQ(6U, v4.size());
}
