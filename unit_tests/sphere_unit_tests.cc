#include <gtest/gtest.h>

#include <v8/math/vector3.hpp>
#include <v8/math/objects/sphere.hpp>
#include <v8/math/containment/containment_sphere.hpp>

extern const float C_Epsilon;
using namespace v8::math;

TEST(sphere_tests, containment) {
    sphereF sphere(vector3F(2.0f, 9.0f, -3.0f), 10.0f);

    const vector3F p0(3.0f, 4.0f, -1.0f);
    EXPECT_TRUE(contains_object(sphere, p0));

    const vector3F p1(12.0f, -2.0f, 1.0f);
    EXPECT_FALSE(contains_object(sphere, p1));
}
