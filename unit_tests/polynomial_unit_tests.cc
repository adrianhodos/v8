#include <iterator>
#include <gtest/gtest.h>
#include <v8/base/count_of.hpp>
#include <v8/math/numerical_analysis/polynomial.hpp>
#include "test_config.hpp"

using namespace v8::base;
using namespace v8::math;
using namespace std;

class polynomial_tests : public ::testing::Test {
protected :
    polynomialF poly_;

public :
    polynomial_tests()
        : ::testing::Test(), poly_(3.0f, 4.0f, 5.0f, 6.0f) {}
};

TEST_F(polynomial_tests, constructors) {
    EXPECT_NEAR(3.0f, poly_[3], C_Epsilon);
    EXPECT_NEAR(4.0f, poly_[2], C_Epsilon);
    EXPECT_NEAR(5.0f, poly_[1], C_Epsilon);
    EXPECT_NEAR(6.0f, poly_[0], C_Epsilon);

    EXPECT_EQ(3, poly_.get_degree());
}

TEST_F(polynomial_tests, construct_from_array) {
    const float poly_coeffs[] = {
        2.0f, 5.0f, 8.0f, 12.0f, -3.0f
    };

    auto lambda_get_tmp_poly = [](const float* coeffs, size_t size) {
            return polynomialF(coeffs, size);
    };

    poly_ = lambda_get_tmp_poly(poly_coeffs, count_of_array(poly_coeffs));

    EXPECT_EQ(4, poly_.get_degree());

    for (int i = 0; i < 5; ++i) {
        EXPECT_NEAR(poly_coeffs[i], poly_[i], C_Epsilon);
    }
}

TEST_F(polynomial_tests, self_add) {
    const float poly_coeffs[] = {
        -3.0f, 8.0f, -4.0f, 10.0f, 10.0f, 2.0f, 5.0f, -1.0f, 2.0f, 3.0f
    };

    polynomialF poly2(poly_coeffs, count_of_array(poly_coeffs));

    const float expected_coeffs[] = {
        3.0f, 13.0f, 0.0f, 13.0f, 10.0f, 2.0f, 5.0f, -1.0f, 2.0f, 3.0f
    };

    poly_ += poly2;
    EXPECT_EQ(9, poly_.get_degree());
    for (int i = 0; i < 10; ++i) {
        EXPECT_NEAR(expected_coeffs[i], poly_[i], C_Epsilon);
    }

    const float poly_coeffs2[] = {
        5.0f, 10.0f
    };

    poly2.assign(poly_coeffs2, count_of_array(poly_coeffs2));
    poly_ += poly2;

    const float expected_coeffs2[] = {
        8.0f, 23.0f
    };
    for (int i = 0; i < 2; ++i) {
        EXPECT_NEAR(expected_coeffs2[i], poly_[i], C_Epsilon);
    }

    const float poly_coeffs3[] = {
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, -2.0f, -3.0f
    };
    poly2.assign(poly_coeffs3, count_of_array(poly_coeffs3));

    const float expected_coeffs3[] = {
        8.0f, 23.0f, 0.0f, 13.0f, 10.0f, 2.0f, 5.0f, -1.0f
    };

    poly_ += poly2;

    EXPECT_EQ(7, poly_.get_degree());
    for (int i = 0; i < 8; ++i) {
        EXPECT_NEAR(expected_coeffs3[i], poly_[i], C_Epsilon);
    }
}

TEST_F(polynomial_tests, divide_multiply) {
    //
    // 3.0f, 4.0f, 5.0f, 6.0f
    const float expected_coeffs[] = {
        12.0f, 10.0f, 8.0f, 6.0f
    };

    poly_ *= 2.0f;

    for (int i = 0; i < 4; ++i) {
        EXPECT_NEAR(expected_coeffs[i], poly_[i], C_Epsilon);
    }

    const float expected_coeffs2[] = {
        6.0f, 5.0f, 4.0f, 3.0f
    };

    poly_ /= 2.0f;

    for (int i = 0; i < 4; ++i) {
        EXPECT_NEAR(expected_coeffs2[i], poly_[i], C_Epsilon);
    }
}

TEST_F(polynomial_tests, evaluate) {
    //
    // 8, 4, 2, 1
    const float expected_value = 8.0f * 3.0f + 4.0f * 4.0f + 2.0f * 5.0f + 6.0f;
    EXPECT_NEAR(expected_value, poly_.evaluate(2.0f), C_Epsilon);
}

TEST_F(polynomial_tests, evaluate_derivative) {
    const float expected_value = 9.0f * 100.0f + 8.0f * 10.0f + 5.0f;
    EXPECT_NEAR(expected_value, poly_.evaluate_derivative(10.0f), C_Epsilon);

    float data[] = { 5.0f, -8.0f };
    poly_.assign(data, static_cast<size_t>(1));
    EXPECT_NEAR(0.0f, poly_.evaluate_derivative(10.0f), C_Epsilon);

    poly_.assign(data, count_of_array(data));
    EXPECT_NEAR(-8.0f, poly_.evaluate_derivative(64.0f), C_Epsilon);
}

TEST_F(polynomial_tests, evaluate_integral) {
    EXPECT_NEAR(18288.0f, poly_.evaluate_integral(12.0f), C_Epsilon);
    EXPECT_NEAR(4102.25f, poly_.evaluate_integral(-9.0f, 5.0f), C_Epsilon);
}

TEST_F(polynomial_tests, roots_1) {
    float eq_root;
    int root_count = polynomialF::get_roots(3.0f, 9.0f, &eq_root);
    EXPECT_NEAR(-3.0f, eq_root, C_Epsilon);
    EXPECT_EQ(1, root_count);
}

TEST_F(polynomial_tests, roots_2) {
    float roots[2];

    int root_count = polynomialF::get_roots(1.0f, -3.0f, -4.0f, roots);

    EXPECT_NEAR(4.0f, roots[0], C_Epsilon);
    EXPECT_NEAR(-1.0f, roots[1], C_Epsilon);
    EXPECT_EQ(2, root_count);

    root_count = polynomialF::get_roots(1.0f, -4.0f, 4.0f, roots);
    EXPECT_NEAR(2.0f, roots[0], C_Epsilon);
    EXPECT_EQ(1, root_count);

    root_count = polynomialF::get_roots(1.0f, 3.0f, 4.0f, roots);
    EXPECT_EQ(0, root_count);
}

TEST_F(polynomial_tests, roots_3) {
    float poly_roots[3];
    int root_count = polynomialF::get_roots(-5.0f, 4.0f, 3.0f, -2.0f, poly_roots);

    EXPECT_EQ(3, root_count);
    const float exp_roots[] = { -0.74031f, 0.54031f, 1.0f };
    auto find_root_fn = [](const float* first, const float* last, float target) -> bool {
        while (first != last) {
            if (operands_eq(*first, target))
                return true;
            ++first;
        }
        return false;
    };

    EXPECT_TRUE(find_root_fn(begin(exp_roots), end(exp_roots), poly_roots[0]));
    EXPECT_TRUE(find_root_fn(begin(exp_roots), end(exp_roots), poly_roots[1]));
    EXPECT_TRUE(find_root_fn(begin(exp_roots), end(exp_roots), poly_roots[2]));

    root_count = polynomialF::get_roots(5.0f, 4.0f, 3.0f, -2.0f, poly_roots);
    EXPECT_EQ(1, root_count);
    EXPECT_NEAR(0.38097f, poly_roots[0], C_Epsilon);
}
