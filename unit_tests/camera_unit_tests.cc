#include <gtest/gtest.h>

#include <v8/math/camera.hpp>
#include <v8/math/math_constants.hpp>
#include <v8/math/matrix4X4.hpp>
#include <v8/math/vector3.hpp>
#include <v8/math/objects/plane.hpp>

using namespace v8::math;

extern const float C_Epsilon;

class CameraTests : public ::testing::Test {
protected :
    camera  cam_;

    void SetUp() {
        cam_.look_at(vector3F(10.0f, 10.0f, -10.0f), vector3F::unit_y, 
                     vector3F(0.0f, 0.0f, 10.0f));
        cam_.set_symmetric_frustrum_perspective(
            45.0f, 1650.0f / 1080.0f, 10.0f, 1000.0f);
    }

    void TearDown() {}
};

TEST_F(CameraTests, view_frame) {
    const vector3F exp_viewframe[] = {
        //
        // direction
        vector3F(-0.4082f, -0.4082f, 0.8164f),
        //
        // up
        vector3F(-0.1825f, 0.9128f, 0.3651f),
        //
        // right
        vector3F(0.8944f, 0.0f, 0.4472f)
    };

    const vector3F actual_viewframe[] = {
        cam_.get_direction_vector(),
        cam_.get_up_vector(),
        cam_.get_right_vector()
    };

    for (size_t i = 0; i < 3; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            EXPECT_NEAR(exp_viewframe[i].elements_[j], 
                        actual_viewframe[i].elements_[j], C_Epsilon);
        }
    }
}

TEST_F(CameraTests, view_matrix) {
    const float expected_values[] = {
        0.8944f, 0.0f, 0.4472f, -4.4721f,
        -0.1825f, 0.9128f, 0.3651f, -3.6514f,
        -0.4082f, -0.4082f, 0.8164f, 16.3299f,
        0.0f, 0.0f, 0.0f, 1.0f
    };

    const matrix_4X4F actual_view_mtx(cam_.get_view_transform());
    for (size_t i = 0; i < 16; ++i) {
        EXPECT_NEAR(expected_values[i], 
                    actual_view_mtx.elements_[i], C_Epsilon);
    }
}

TEST_F(CameraTests, frustrum_parameters) {

    EXPECT_NEAR(cam_.get_dmax(), 1000.0f, C_Epsilon);
    EXPECT_NEAR(cam_.get_dmin(), 10.0f, C_Epsilon);

    EXPECT_NEAR(cam_.get_umax(), 4.142135f, C_Epsilon);
    EXPECT_NEAR(cam_.get_umin(), -4.142135f, C_Epsilon);

    EXPECT_NEAR(cam_.get_rmax(), 6.328258f, C_Epsilon);
    EXPECT_NEAR(cam_.get_rmin(), -6.328258f, C_Epsilon);
}

TEST_F(CameraTests, projection_perspective) {
    cam_.set_projection_type(v8::math::Projection::Perspective);

    const float expected_proj_mtx_values[] = {
        1.580f, 0.0f, 0.0f, 0.0f,
        0.0f, 2.414f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.010f, -10.101f,
        0.0f, 0.0f, 1.0f, 0.0f
    };

    const matrix_4X4F expected_projection(expected_proj_mtx_values, 16);
    const matrix_4X4F actual_projection(cam_.get_projection_transform());

    for (size_t i = 0; i < 16; ++i) {
        EXPECT_NEAR(expected_projection.elements_[i], 
                    actual_projection.elements_[i], C_Epsilon);
    }
}

TEST_F(CameraTests, projection_ortho_parallel) {
    cam_.set_projection_type(v8::math::Projection::Orthographic);

    const float expected_values[] = {
        0.1580f, 0.0f, 0.0f, 0.0f,
        0.0f, 0.2414f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0010f, -0.0101f,
        0.0f, 0.0f, 0.0f, 1.0f
    };

    const matrix_4X4F expected_projection(expected_values, 16);
    const matrix_4X4F actual_projection(cam_.get_projection_transform());

    for (size_t i = 0; i < 16; ++i) {
        EXPECT_NEAR(expected_projection.elements_[i], 
                    actual_projection.elements_[i], C_Epsilon);
    }
}

TEST_F(CameraTests, frustrum_vertices) {
    const vector3F expected_vertices[] = {
        vector3F(-0.4988f, 9.6987f, -3.1526f),
        vector3F(10.8214f, 9.6987f, 2.5075f),        
        vector3F(1.0135f, 2.1362f, -6.1776f),
        vector3F(12.3339f, 2.1362f, -0.5174f),
        vector3F(-1039.8895f, -20.1248f, 674.7376f),
        vector3F(92.1436f, -20.1248f, 1240.7542f),
        vector3F(-888.6402f, -776.3717f, 372.2388f),
        vector3F(243.3930f, -776.3717f, 938.2554f)
    };

    vector3F actual_vertices[8];
    cam_.copy_frustrum_vertices(actual_vertices);

    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            EXPECT_NEAR(expected_vertices[i].elements_[j], 
                        actual_vertices[i].elements_[j], C_Epsilon);
        }
    }
}

TEST_F(CameraTests, frustrum_planes) {
    const plane3F expected_planes[] = {
        //
        // near
        plane3F(vector3F(-0.4082f, -0.4082f, 0.8164f), 6.3299f),
        //
        // far
        plane3F(vector3F(0.4082f, 0.4082f, -0.8164f), 983.6700f),
        //
        // top
        plane3F(vector3F(0.01244f, -0.9996f, -0.0248f), 9.6227f),
        //
        // bottom
        plane3F(vector3F(-0.3249f, 0.6871f, 0.6498f), 2.8756f),
        //
        // left
        plane3F(vector3F(0.5374f, -0.2183f, 0.8145f), 4.9533f),
        //
        // right
        plane3F(vector3F(-0.9741f, -0.2183f, 0.0587f), 12.5113f)
    };

    plane3F actual_planes[6];
    cam_.extract_frustrum_planes(actual_planes);

    for (size_t i = 0; i < 6; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            EXPECT_NEAR(expected_planes[i].normal_.elements_[j], 
                        actual_planes[i].normal_.elements_[j], C_Epsilon);
        }
        EXPECT_NEAR(expected_planes[i].offset_, 
                    actual_planes[i].offset_, C_Epsilon);
    }
}

TEST_F(CameraTests, frustrum_planes2) {
    const plane3F expected_planes[] = {
        //
        // near
        plane3F(vector3F(-0.4082f, -0.4082f, 0.8164f), 6.3299f),
        //
        // far
        plane3F(vector3F(0.4082f, 0.4082f, -0.8164f), 983.6700f),
        //
        // top
        plane3F(vector3F(0.01244f, -0.9996f, -0.0248f), 9.6227f),
        //
        // bottom
        plane3F(vector3F(-0.3249f, 0.6871f, 0.6498f), 2.8756f),
        //
        // left
        plane3F(vector3F(0.5374f, -0.2183f, 0.8145f), 4.9533f),
        //
        // right
        plane3F(vector3F(-0.9741f, -0.2183f, 0.0587f), 12.5113f)
    };

    plane3F actual_planes[6];
    cam_.extract_frustrum_planes2(actual_planes);

    for (size_t i = 0; i < 6; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            EXPECT_NEAR(expected_planes[i].normal_.elements_[j], 
                        actual_planes[i].normal_.elements_[j], C_Epsilon);
        }
        EXPECT_NEAR(expected_planes[i].offset_, 
                    actual_planes[i].offset_, C_Epsilon);
    }
}
