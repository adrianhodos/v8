#include <gtest/gtest.h>
#include <v8/base/count_of.hpp>
#include <v8/math/vector3.hpp>
#include <v8/math/objects/capsule3.hpp>
#include <v8/math/objects/segment3.hpp>
#include <v8/math/objects/sphere.hpp>
#include <v8/math/containment/containment_capsule.hpp>
#include <v8/math/containment/containment_capsule3.hpp>
#include "test_config.hpp"

using namespace v8::base;
using namespace v8::math;

class capsule_tests : public ::testing::Test {
protected :
    capsule3F   caps_;

    void SetUp() {
        caps_ = capsule3F(segment3F(vector3F::zero, vector3F::unit_x, 8.0f),
                          4.0f);
    }
};

TEST_F(capsule_tests, containment_point) {
    const vector3F p0(3.0f, 9.0f, 0.0f);
    EXPECT_FALSE(contains_object(caps_, p0));

    const vector3F p1(3.0f, 3.0f, 0.0f);
    EXPECT_TRUE(contains_object(caps_, p1));
}

TEST_F(capsule_tests, containment_sphere) {
    const sphereF sph0(vector3F::unit_y, 10.0f);
    EXPECT_FALSE(contains_object(caps_, sph0));

    const sphereF sph1(vector3F(12.0f, 10.0f, 10.0f), 4.0f);
    EXPECT_FALSE(contains_object(caps_, sph1));

    const sphereF sph2(vector3F::zero, 4.0f);
    EXPECT_TRUE(contains_object(caps_, sph2));
}

TEST_F(capsule_tests, containment_capsule) {
    const capsule3F cap0(segment3F(vector3F(-4.0f, 0.0f, 0.0f),
                                   vector3F::unit_x, 2.0f), 2.0f);

    EXPECT_TRUE(contains_object(caps_, cap0));
    EXPECT_FALSE(contains_object(cap0, caps_));

    const capsule3F cap1(segment3F(vector3F(0.0f, 10.0f, 0.0f),
                                   vector3F::unit_z, 8.0f), 2.0f);
    EXPECT_FALSE(contains_object(caps_, cap1));
}
