#include <gtest/gtest.h>
#include <iterator>
#include <v8/base/count_of.hpp>
#include <v8/math/objects/polygon2.hpp>
#include <v8/math/vector2.hpp>
#include "test_config.hpp"
/*
using namespace v8::math;

namespace {

const vector2F poly_verts[] = {
    vector2F(-4.74f, -9.34f), vector2F(7.94f, -8.49f), vector2F(14.89f, 6.49f),
    vector2F(13.02f, 19.71f), vector2F(-7.04f, 16.39f)
};

}

class polygon2_unit_tests : public ::testing::Test {
public :
    polygon2_unit_tests()
        : ::testing::Test(),
          poly_(poly_verts, dimension_of(poly_verts)) {}

protected :
    v8::math::polygon2F poly_;
};

TEST_F(polygon2_unit_tests, sanity_checks) {
    EXPECT_EQ(dimension_of(poly_verts), poly_.get_vertex_count());

    v8_size_t i = 0;
    v8_size_t num_verts = dimension_of(poly_verts);

    while (i < num_verts) {
        EXPECT_TRUE(poly_verts[i] == poly_[i]);
        ++i;
    }
}

TEST_F(polygon2_unit_tests, properties) {
    EXPECT_NEAR(498.4647f, poly_.area(), C_Epsilon);

    vector2F p_centroid;
    float p_area;
    poly_.centroid(&p_centroid, &p_area);

    EXPECT_NEAR(498.4647f, p_area, C_Epsilon);
    EXPECT_NEAR(3.5701f, p_centroid.x_, C_Epsilon);
    EXPECT_NEAR(5.6185f, p_centroid.y_, C_Epsilon);

    EXPECT_NEAR(88.739f, poly_.perimeter(), C_Epsilon);
}
*/