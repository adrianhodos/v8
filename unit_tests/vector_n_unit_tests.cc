#include <algorithm>

#include <gtest/gtest.h>
#include <v8/base/count_of.hpp>
#include <v8/math/vector_N.hpp>

#include "test_config.hpp"

using namespace v8::base;
using namespace v8::math;
using namespace std;

typedef vector_N<int>    vector_NI;
typedef vector_N<float>  vector_NF;

class vector_n_tests : public ::testing::Test {
protected :
    vector_NI  vec_;

    void SetUp() {
        const int init_values[] = { 1, 2, 3, 4 };
        vec_ = vector_NI(init_values, count_of_array(init_values));
    }

public :
    vector_n_tests() : vec_((size_t)4) {
    }
};

TEST_F(vector_n_tests, init) {
    const int exp_values[] = { 1, 2, 3, 4 };
    for (size_t i = 0; i < 4U; ++i)
        EXPECT_EQ(exp_values[i], vec_[i + 1]);

    const int exp_values2[] = { 9, 5, 2, 6 };
    auto lambda_tmp = [](const int* values, size_t size) -> vector_NI {
            return vector_NI(values, size);
    };

vec_ = lambda_tmp(exp_values2, count_of_array(exp_values2));

    for (size_t i = 0; i < 4U; ++i)
        EXPECT_EQ(exp_values2[i], vec_[i + 1]);
}

TEST_F(vector_n_tests, self_assign_ops) {
    const int values[] = { 0, 2, 4, 8 };
    const vector_NI v2(values, count_of_array(values));

    vec_ += v2;
    const int exp_val_add[] = { 1, 4, 7, 12 };
    for (size_t i = 0; i < 4U; ++i) {
        EXPECT_EQ(exp_val_add[i], vec_[i + 1]);
    }

    vec_ -= v2;
    const int exp_val_sub[] = { 1, 2, 3, 4 };
    for (size_t i = 0; i < 4U; ++i) {
        EXPECT_EQ(exp_val_sub[i], vec_[i + 1]);
    }

    const int exp_val_mul[] = { 3, 6, 9, 12 };
    vec_ *= 3;

    for (size_t i = 0; i < 4U; ++i) {
        EXPECT_EQ(exp_val_mul[i], vec_[i + 1]);
    }

    const int exp_val_div[] = { 1, 2, 3, 4 };
    vec_ /= 3;

    for (size_t i = 0; i < 4U; ++i) {
        EXPECT_EQ(exp_val_div[i], vec_[i + 1]);
    }

    const int component_mul[] = { 3, 2, 4, 2 };
    const int exp_val_cmp_mul[] = { 3, 4, 12, 8 };
    vec_ ^= vector_NI(component_mul, count_of_array(component_mul));

    for (size_t i = 0; i < 4U; ++i) {
        EXPECT_EQ(exp_val_cmp_mul[i], vec_[i + 1]);
    }
}

TEST_F(vector_n_tests, eq_neq_ops) {
    vector_NI v2(vec_);

    EXPECT_TRUE(vec_ == v2);

    v2 = -v2;

    EXPECT_TRUE(vec_ != v2);
}

TEST_F(vector_n_tests, dot_product) {
    const int init_val[] = { 4, 3, 9, 10 };
    const vector_NI v0(init_val, count_of_array(init_val));

    EXPECT_EQ(77, dot_product(vec_, v0));
}

TEST_F(vector_n_tests, length_square_lenght_normalize) {
    const float init_values[] = { 1.0f, 2.0f, 3.0f, 4.0f };
    vector_NF v0(init_values, count_of_array(init_values));

    EXPECT_NEAR(30.0f, v0.length_squared(), C_Epsilon);
    EXPECT_NEAR(5.4772f, v0.length(), C_Epsilon);

    const float exp_norm[] = { 0.1825f, 0.3651f, 0.5477f, 0.7302f };
    v0.normalize();

    for (size_t i = 0; i < 4U; ++i)
        EXPECT_NEAR(exp_norm[i], v0[i + 1], C_Epsilon);
}

#if defined(V8_COMPILER_IS_GCC) || defined(V8_COMPILER_IS_CLANG) \
    || defined(V8_COMPILER_IS_MINGW)

TEST_F(vector_n_tests, initializer_list) {
    vector_NI v0 = { 2, 5, 9, 10 };
    const int exp_val[] = { 2, 5, 9, 10 };
    for (size_t i = 0; i < 4; ++i)
        EXPECT_EQ(exp_val[i], v0[i + 1]);
}

#endif
