#include <gtest/gtest.h>
#include "test_config.hpp"
#include "v8/math/angle_units.hpp"

TEST(angle_units, degrees_to_rads) {

    v8::math::v8_degree_t deg(1.0f);

    v8::math::v8_radian_t rad(v8::math::degrees_to_radians(deg));

    EXPECT_NEAR(0.0175f, rad.base_type_value(), C_Epsilon);

    deg = v8::math::v8_degree_t(23.0f);

    rad = v8::math::degrees_to_radians(deg);

    EXPECT_NEAR(0.4014f, rad.base_type_value(), C_Epsilon);

}

TEST(angle_units, rads_to_degrees) {

    v8::math::v8_radian_t rad(1.0f);

    v8::math::v8_degree_t deg(v8::math::radians_to_degrees(rad));

    EXPECT_NEAR(57.2958f, deg.base_type_value(), C_Epsilon);

    rad = v8::math::v8_radian_t(2.5f);

    deg = v8::math::radians_to_degrees(rad);

    EXPECT_NEAR(143.2394f, deg.base_type_value(), C_Epsilon);
}
