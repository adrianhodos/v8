#include <gtest/gtest.h>

#include <v8/v8.hpp>
#include <v8/base/scope_exit.hpp>

#if defined(V8_COMPILER_HAS_CXX11_LAMBDAS)

TEST(scope_exit_tests, basic) {
    v8_int_t var0 = 0;
    v8_int_t var1 = 0;
    {
        V8_ON_SCOPE_EXIT((++var0); (--var1));
    }

    EXPECT_EQ(1, var0);
    EXPECT_EQ(-1, var1);
}

TEST(scope_exit_tests, rollback) {
    v8_int_t var0 = 0;
    v8_int_t var1 = 0;

    {
        V8_ON_SCOPE_EXIT_NAMED(increment_vars, (++var0); (++var1));

        increment_vars = false;
    }

    EXPECT_EQ(0, var0);
    EXPECT_EQ(0, var1);
}

#endif /* V8_COMPILER_HAS_CXX11_LAMBDAS */
