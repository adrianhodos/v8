#include <algorithm>
#include <gtest/gtest.h>
#include <v8/math/fixed_array_3d.hpp>

using namespace std;
using namespace v8::math;

class FixedArray3DUnitTests : public ::testing::Test {
protected :
    fixed_array_3D<int, 2, 2, 4>    arr_;

    void SetUp() {
        int ssq_start = 0;
        generate_n(begin(arr_), arr_.size(), [&ssq_start]() {
            return ssq_start++;
        });
    }
};

TEST_F(FixedArray3DUnitTests, iterators) {
    auto itr_first = begin(arr_);
    auto itr_last = end(arr_);

    int expected_values[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                              15 };

    int i = 0;
    while (itr_first != itr_last) {
        EXPECT_EQ(expected_values[i], *itr_first);
        ++i;
        ++itr_first;
    }
}

TEST_F(FixedArray3DUnitTests, indexed_access) {
    int expected_values[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                              15 };

    int i = 0;
    for (size_t y = 0; y < arr_.dimension_y(); ++y) {
        for (size_t x = 0; x < arr_.dimension_x(); ++x) {
            for (size_t z = 0; z < arr_.dimension_z(); ++z) {
                EXPECT_EQ(expected_values[i], arr_(x, y, z));
                ++i;
            }
        }
    }
}

TEST_F(FixedArray3DUnitTests, wrapping) {
    fixed_array_3D<int, 2, 2, 4, oob_wrap_policy> arr_wrap;
    copy(begin(arr_), end(arr_), begin(arr_wrap));

    EXPECT_EQ(arr_(0, 0, 2), arr_wrap(3, 3, 8));
    EXPECT_EQ(arr_(0, 0, 0), arr_wrap(6, 6, 12));
}

TEST_F(FixedArray3DUnitTests, clamping) {
    fixed_array_3D<int, 2, 2, 4, oob_clamp_policy> arr_clamp;
    copy(begin(arr_), end(arr_), begin(arr_clamp));

    EXPECT_EQ(arr_(1, 1, 3), arr_clamp(4, 8, 15));
}
