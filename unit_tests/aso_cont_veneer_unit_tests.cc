﻿#include <set>
#include <map>
#include <functional>

#include <gtest/gtest.h>
#include <v8/v8.hpp>
#include <v8/base/count_of.hpp>
#include <v8/base/associative_container_veneer.hpp>

namespace {

struct null_destruct_fn {
    void operator()(int) const {
    }
};

typedef std::set<int>   intset_t;

typedef v8::base::associative_container_veneer<intset_t, null_destruct_fn>
    int_set_veneer_t;

int_set_veneer_t make_tmp_veneer() {
    return int_set_veneer_t();
}

}

TEST(asso_cont_veneer_tests, constructors) {
    //
    // Default constructor
    int_set_veneer_t v0;
    EXPECT_EQ(v8_size_t(0), v0.size());

#if defined(V8_COMPILER_HAS_CXX11_MOVE_SEMANTICS)
    int_set_veneer_t v2(make_tmp_veneer());
    EXPECT_EQ(0U, v2.size());

    v2 = make_tmp_veneer();
    EXPECT_EQ(0U, v2.size());
#endif    

#if defined(V8_COMPILER_HAS_CXX11_INITIALIZER_LISTS)
    int_set_veneer_t v3 { 0, 1, 2, 3, 4 };
    EXPECT_EQ(5U, v3.size());

    v3 = { 1, 2 };
    EXPECT_EQ(2U, v3.size());
#endif    

    int arr[] = { 0, 1, 2, 3, 4, 5 };

    int_set_veneer_t v4(&arr[0], &arr[0] + dimension_of(arr));
    EXPECT_EQ(6U, v4.size());
}
