#include <gtest/gtest.h>
#include <v8/base/count_of.hpp>
#include <v8/math/objects/axis_aligned_bounding_box3.hpp>
#include <v8/math/containment/containment_aabb.hpp>

#include "test_config.hpp"

using namespace v8::base;
using namespace v8::math;

class aabb_tests : public ::testing::Test {
protected :
    aabb3F  bbox_;

    void SetUp() {
        bbox_ = aabb3F(vector3F(-10.0f, -10.0f, -10.0f),
                       vector3F(20.0f, 40.0f, 10.0f));
    }
};

TEST_F(aabb_tests, center_and_extents) {
    const float exp_center[] = {
        5.0f, 15.0f, 0.0f
    };

    const float exp_extents[] = {
        15.0f, 25.0f, 10.0f
    };

    vector3F center;
    vector3F extents;
    bbox_.get_center_and_extents(&center, &extents);

    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(exp_center[i], center.elements_[i], C_Epsilon);
    }

    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(exp_extents[i], extents.elements_[i], C_Epsilon);
    }
}

TEST_F(aabb_tests, init_from_points) {
    struct vertex_t {
        vector3F v_pos;
        vector3F v_texcoord;

        vertex_t(float x, float y, float z)
            : v_pos(x, y, z) {}
    };

    const vertex_t init_data[] = {
        vertex_t(-5.0f, 0.0f, 5.0f),
        vertex_t(-3.0f, 1.0f, 9.0f),
        vertex_t(12.0f, 96.0f, -40.0f),
        vertex_t(15.0f, 21.0f, -15.0f),
        vertex_t(6.0f, 9.0f, 3.0f),
        vertex_t(8.0f, 12.0f, -10.0f),
        vertex_t(-5.0f, -1.0f, -2.0f),
        vertex_t(6.0f, 6.0f, 7.0f),
        vertex_t(100.0f, -120.0f, 31.0f),
        vertex_t(-128.0f, 48.0f, 16.0f)
    };

    aabb_from_points(reinterpret_cast<const vector3F*>(init_data),
                     count_of_array(init_data), sizeof(vertex_t),
                     &bbox_);

    const float exp_aabb_min[] = {
        -128.0f, -120.0f, -40.0f
    };

    const float exp_aabb_max[] = {
        100.0f, 96.0f, 31.0f
    };

    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(exp_aabb_min[i], bbox_.min_point_.elements_[i], C_Epsilon);
    }
    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(exp_aabb_max[i], bbox_.max_point_.elements_[i], C_Epsilon);
    }
}

TEST_F(aabb_tests, containment) {
    const vector3F Q0(-5.0f, 5.0f, 12.0f);
    EXPECT_FALSE(contains_object(bbox_, Q0));

    EXPECT_TRUE(contains_object(bbox_, bbox_.min_point_));
    EXPECT_TRUE(contains_object(bbox_, bbox_.max_point_));

    const vector3F Q1(0.0f, 20.0f, 5.0f);
    EXPECT_TRUE(contains_object(bbox_, Q1));
}
