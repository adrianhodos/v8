#include <gtest/gtest.h>

#include <v8/v8.hpp>
#include <v8/base/debug_helpers.hpp>
#include <v8/math/math_utils.hpp>

#include "test_config.hpp"

TEST(math_utils_tests, equality_inequality) {
    EXPECT_TRUE(v8::math::operands_eq(1.03f, 1.03f));
    EXPECT_TRUE(v8::math::operands_ge(2.5f, 0.005f));
    EXPECT_TRUE(v8::math::operands_le(-3.25f, 0.0054f));

    EXPECT_TRUE(v8::math::is_zero(1.0e-8f));

    EXPECT_EQ(-1, v8::math::signum(-4.55f));
    EXPECT_EQ(+1, v8::math::signum(+4.55f));

    EXPECT_NEAR(0.5f, v8::math::inv_sqrt(4.0f), C_Epsilon);
}

TEST(math_utils_tests, min_max_clamp) {
    EXPECT_NEAR(0.5f, v8::math::min(0.5f, 10.0f), C_Epsilon);
    EXPECT_NEAR(10.0f, v8::math::max(0.5f, 10.0f), C_Epsilon);

    EXPECT_NEAR(0.5f, v8::math::clamp(0.5f, 0.0f, 1.0f), C_Epsilon);

    EXPECT_NEAR(0.0f, v8::math::clamp(-1.0f, 0.0f, 1.0f), C_Epsilon);

    EXPECT_NEAR(1.0f, v8::math::clamp(+2.0f, 0.0f, 1.0f), C_Epsilon);
}

TEST(math_utils_tests, random_number_generating) {
    const float gen_num = v8::math::random_number_in_interval(1.0f, 10.0f);

    EXPECT_TRUE(v8::math::operands_ge(gen_num, 1.0f));
    EXPECT_TRUE(v8::math::operands_le(gen_num, 10.0f));
}
