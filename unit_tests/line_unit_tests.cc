#include <gtest/gtest.h>

#include <v8/math/vector3.hpp>
#include <v8/math/objects/line3.hpp>
#include <v8/math/distance/distance_point_line.hpp>

extern const float C_Epsilon;

using namespace v8::math;

class line_tests : public ::testing::Test {
protected :
    line3F  ln_;
    vector3F p0_;
    vector3F p1_;

    void SetUp() {
        p0_ = vector3F(15.0f, -20.0f, 4.0f);
        p1_ = vector3F(25.0f, -36.0f, -19.0f);
        ln_ = line3F::make_from_points(p0_, p1_);
    }
};

TEST_F(line_tests, initialization) {
    const float exp_val_dirvec[] = {
        0.3361f, -0.5378f, -0.7731f
    };

    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(exp_val_dirvec[i], ln_.direction_.elements_[i], C_Epsilon);
    }

    EXPECT_TRUE(ln_.origin_ == p0_);
}

TEST_F(line_tests, distance_point) {
    const vector3F pointq(-4.0f, -10.0f, -2.0f);

    float tval_closest;
    float dst_squared = squared_distance(pointq, ln_, &tval_closest);

    EXPECT_NEAR(-7.126f, tval_closest, C_Epsilon);
    EXPECT_NEAR(446.215f, dst_squared, C_Epsilon);
}
