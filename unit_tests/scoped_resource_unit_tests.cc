#include <gtest/gtest.h>

#include <v8/v8.hpp>
#include <v8/base/scoped_resource.hpp>
#include <v8/base/resource_policies/refcount_ownership.hpp>
#include <v8/base/resource_policies/ref_link_ownership.hpp>

class BaseClass {
public :
    BaseClass() {
        ++created_;
    }

    virtual ~BaseClass() {
        ++destroyed_;
    }

    static v8_size_t created() {
        return created_;
    }

    static v8_size_t destroyed() {
        return destroyed_;
    }

    static void reset_object_count() {
        created_ = destroyed_ = 0U;
    }

    virtual v8_size_t dummy_fn() const {
        return 0;
    }

private :
    static v8_size_t    created_;
    static v8_size_t    destroyed_;
};

class DerivedClass : public BaseClass {
public :

    v8_size_t dummy_fn() const {
        return 1;
    }
};

v8_size_t BaseClass::created_;
v8_size_t BaseClass::destroyed_;

typedef v8::base::scoped_resource
<
    BaseClass,
    v8::base::default_pointer_storage,
    v8::base::ref_count_st_ownership
> sp_baseclass_t;

typedef v8::base::scoped_resource
<
    DerivedClass,
    v8::base::default_pointer_storage,
    v8::base::ref_count_st_ownership
> sp_derivedclass_t;

typedef v8::base::scoped_resource
<
    BaseClass,
    v8::base::default_pointer_storage,
    v8::base::ref_linked_ownership
> sp_baseclass_rl_t;

typedef v8::base::scoped_resource
<
    DerivedClass,
    v8::base::default_pointer_storage,
    v8::base::ref_linked_ownership
> sp_derivedclass_rl_t;

TEST(scoped_resource_tests, test0) {
    BaseClass::reset_object_count();

    {
        sp_baseclass_t sp0(new BaseClass());
        sp_baseclass_t sp1(sp0);

        EXPECT_EQ(v8::base::scoped_res_get(sp0), v8::base::scoped_res_get(sp1));
    }

    EXPECT_EQ(1U, BaseClass::created());
    EXPECT_EQ(1U, BaseClass::destroyed());

    {
        sp_derivedclass_t sp0(new DerivedClass());
        sp_baseclass_t sp1(sp0);

        EXPECT_EQ(1U, sp1->dummy_fn());
    }

    EXPECT_EQ(2U, BaseClass::created());
    EXPECT_EQ(2U, BaseClass::destroyed());
}

TEST(scoped_resource_tests, test1) {
    BaseClass::reset_object_count();

    {
        sp_baseclass_rl_t sp0(new BaseClass());
        sp_baseclass_rl_t sp1(sp0);

        EXPECT_EQ(v8::base::scoped_res_get(sp0), v8::base::scoped_res_get(sp1));
    }

    EXPECT_EQ(1U, BaseClass::created());
    EXPECT_EQ(1U, BaseClass::destroyed());

    {
        sp_derivedclass_rl_t sp0(new DerivedClass());
        sp_baseclass_rl_t sp1(sp0);

        EXPECT_EQ(1U, sp1->dummy_fn());
    }

    EXPECT_EQ(2U, BaseClass::created());
    EXPECT_EQ(2U, BaseClass::destroyed());
}

TEST(scoped_resource_tests, test2) {
    BaseClass::reset_object_count();

    {
        sp_baseclass_rl_t sp0(new BaseClass());
        sp_baseclass_rl_t sp1(new BaseClass());

        sp1 = sp0;
        sp0 = sp1;

        sp0 = sp_baseclass_rl_t(new BaseClass());
        sp1 = sp0;
        sp0 = sp1;
    }

    EXPECT_EQ(3U, BaseClass::created());
    EXPECT_EQ(3U, BaseClass::destroyed());
}

TEST(scoped_resource_tests, sanity_checking) {
    BaseClass::reset_object_count();

    sp_baseclass_t sp0;

    EXPECT_TRUE(!sp0);
    EXPECT_FALSE(sp0);

    sp0 = sp_baseclass_t(new BaseClass());

    EXPECT_TRUE(sp0);
    EXPECT_FALSE(!sp0);
}

TEST(scoped_resource_tests, access) {
    BaseClass::reset_object_count();

    sp_baseclass_rl_t sp0;

    EXPECT_TRUE(!v8::base::scoped_res_get(sp0));

    BaseClass* bptr = new BaseClass();

    *v8::base::scoped_res_get_pointer(sp0) = bptr;

    EXPECT_EQ(bptr, v8::base::scoped_res_get(sp0));

    v8::base::scoped_res_reset(sp0, nullptr);

    EXPECT_EQ(1U, BaseClass::created());
    EXPECT_EQ(1U, BaseClass::destroyed());
}

TEST(scoped_resource_tests, swap) {
    BaseClass::reset_object_count();

    BaseClass* obj0 = new BaseClass();
    BaseClass* obj1 = new BaseClass();

    sp_baseclass_rl_t sp0(obj0);
    sp_baseclass_rl_t sp1(obj1);

    swap(sp0, sp1);

    EXPECT_EQ(obj0, v8::base::scoped_res_get(sp1));
    EXPECT_EQ(obj1, v8::base::scoped_res_get(sp0));

}

TEST(scoped_resource_tests, swap2) {
    BaseClass::reset_object_count();

    BaseClass* obj0 = new BaseClass();
    BaseClass* obj1 = new BaseClass();

    sp_baseclass_t sp0(obj0);
    sp_baseclass_t sp1(obj1);

    swap(sp0, sp1);

    EXPECT_EQ(obj0, v8::base::scoped_res_get(sp1));
    EXPECT_EQ(obj1, v8::base::scoped_res_get(sp0));

}

TEST(scoped_resource_tests, eq0) {
    BaseClass::reset_object_count();

    sp_baseclass_t sp0(new BaseClass());
    sp_baseclass_t sp1(sp0);

    EXPECT_TRUE(sp0 == sp1);
    EXPECT_FALSE(sp0 != sp1);
}

//TEST(scoped_resource_tests, eq1) {
//    BaseClass::reset_object_count();

//    BaseClass* obj0 = new BaseClass();
//    BaseClass* obj1 = new BaseClass();

//    sp_baseclass_t sp0(obj0);
//    sp_baseclass_t sp1(obj1);

//    EXPECT_TRUE(obj0 == sp0);
//    //EXPECT_TRUE(sp0 == obj0);

//    EXPECT_TRUE(sp1 != obj0);
//    EXPECT_TRUE(obj0 != sp1);
//}

TEST(scoped_resource_tests, eq2) {
    BaseClass::reset_object_count();

    sp_baseclass_rl_t sp0(new BaseClass());
    sp_derivedclass_rl_t sp1(new DerivedClass());

    EXPECT_TRUE(sp0 != sp1);
    EXPECT_FALSE(sp0 == sp1);
}
