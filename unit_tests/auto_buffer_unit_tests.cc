#include <gtest/gtest.h>

#include <v8/v8.hpp>
#include <v8/base/auto_buffer.hpp>

TEST(auto_buffer_tests, constructors0) {
    v8::base::auto_buffer<int, 128U> b0;
    
    EXPECT_EQ(128U, b0.size());
    EXPECT_TRUE(b0.has_static_storage());
}

TEST(auto_buffer_tests, constructor1) {
    v8::base::auto_buffer<int, 32U> b0(64U);
    
    EXPECT_EQ(64U, b0.size());
    EXPECT_FALSE(b0.has_static_storage());
}

TEST(auto_buffer_tests, constructor2) {
    v8::base::auto_buffer<int, 32U> b0(16U, -1);
    
    for (v8_size_t i = 0; i < b0.size(); ++i) {
        EXPECT_EQ(-1, b0[i]);
    }
    
    v8::base::auto_buffer<int, 32U> b1(64U, -1);
    
    for (v8_size_t i = 0; i < b1.size(); ++i) {
        EXPECT_EQ(-1, b1[i]);
    }
}

TEST(auto_buffer_tests, copy_constructor) {
    v8::base::auto_buffer<int, 4U> b0(4U, -1);
    
    v8::base::auto_buffer<int, 4U> b1(b0);
    
    EXPECT_EQ(4U, b1.size());
    EXPECT_EQ(-1, b1[0]);
    EXPECT_EQ(-1, b1[3]);
}

TEST(auto_buffer_tests, resize) {
    v8::base::auto_buffer<int, 4U> b0(4U, -1);
    
    b0.resize(2U);
    
    EXPECT_EQ(2U, b0.size());
    EXPECT_EQ(-1, b0[0]);
    EXPECT_TRUE(b0.has_static_storage());
    
    b0.resize(4U);
    
    EXPECT_EQ(4U, b0.size());
    EXPECT_EQ(-1, b0[0]);
    EXPECT_TRUE(b0.has_static_storage());
    
    b0.resize(8U);
    
    EXPECT_EQ(8U, b0.size());
    EXPECT_EQ(-1, b0[0]);
    EXPECT_FALSE(b0.has_static_storage());
    
    b0.resize(6U);
    
    EXPECT_EQ(6U, b0.size());
    EXPECT_EQ(-1, b0[0]);
    EXPECT_FALSE(b0.has_static_storage());
    
    b0.resize(4U);
    
    EXPECT_EQ(4U, b0.size());
    EXPECT_EQ(-1, b0[0]);
    EXPECT_TRUE(b0.has_static_storage());
}

TEST(auto_buffer_tests, swap0) {
    v8::base::auto_buffer<int, 16U> b0(4U, -1);
    v8::base::auto_buffer<int, 16U> b1(8U, +1);
    
    b0.swap(b1); 
    
    EXPECT_EQ(8U, b0.size());
    EXPECT_EQ(+1, b0[0]);
    
    EXPECT_EQ(4U, b1.size());
    EXPECT_EQ(-1, b1[0]);
    
    EXPECT_TRUE(b0.has_static_storage());
    EXPECT_TRUE(b1.has_static_storage());
}

TEST(auto_buffer_tests, swap1) {
    v8::base::auto_buffer<int, 16U> b0(24U, -1);
    v8::base::auto_buffer<int, 16U> b1(32U, +1);
    
    b0.swap(b1); 
    
    EXPECT_EQ(32U, b0.size());
    EXPECT_EQ(+1, b0[0]);
    
    EXPECT_EQ(24U, b1.size());
    EXPECT_EQ(-1, b1[0]);
    
    EXPECT_FALSE(b0.has_static_storage());
    EXPECT_FALSE(b1.has_static_storage());
}

TEST(auto_buffer_tests, swap2) {
    v8::base::auto_buffer<int, 16U> b0(8U, -1);
    v8::base::auto_buffer<int, 16U> b1(32U, +1);
    
    b0.swap(b1); 
    
    EXPECT_EQ(32U, b0.size());
    EXPECT_EQ(+1, b0[0]);
    
    EXPECT_EQ(8U, b1.size());
    EXPECT_EQ(-1, b1[0]);
    
    EXPECT_FALSE(b0.has_static_storage());
    EXPECT_TRUE(b1.has_static_storage());
}

TEST(auto_buffer_tests, swap3) {
    v8::base::auto_buffer<int, 16U> b0(32U, -1);
    v8::base::auto_buffer<int, 16U> b1(16U, +1);
    
    b0.swap(b1); 
    
    EXPECT_EQ(16U, b0.size());
    EXPECT_EQ(+1, b0[0]);
    
    EXPECT_EQ(32U, b1.size());
    EXPECT_EQ(-1, b1[0]);
    
    EXPECT_TRUE(b0.has_static_storage());
    EXPECT_FALSE(b1.has_static_storage());
}

TEST(auto_buffer_tests, self_assign) {
    v8::base::auto_buffer<int, 16U> b0(16U, -1);
    
    b0 = b0;
    
    EXPECT_EQ(16U, b0.size());
    EXPECT_TRUE(b0.has_static_storage());
    EXPECT_EQ(-1, b0[0]);
}
