#include <gtest/gtest.h>
#include <v8/v8.hpp>
#include <v8/base/count_of.hpp>
#include <v8/math/vector2.hpp>
#include <v8/math/objects/polygon2.hpp>
#include <v8/math/containment/containment_polygon.hpp>

/*
TEST(polygon_containment, point_in_polygon) {
    using namespace v8::math;
    const vector2F poly_vertices[] = {
        vector2F(-1.66f, 3.32f), vector2F(7.62f, 3.78f), vector2F(7.8f, -2.18f),
        vector2F(0.62f, -3.98f), vector2F(-3.24f, -1.32f)
    };

    polygon2F poly(poly_vertices, dimension_of(poly_vertices));

    //
    // inside
    EXPECT_EQ(1, point_in_polygon(poly, vector2F(3.48f, 2.42f)));
    EXPECT_EQ(1, point_in_polygon(poly, vector2F(1.44f, -0.94f)));

    //
    // outside
    EXPECT_EQ(0, point_in_polygon(poly, vector2F(4.58f, -7.0f)));
    EXPECT_EQ(0, point_in_polygon(poly, vector2F(10.0f, 2.0f)));
    EXPECT_EQ(0, point_in_polygon(poly, vector2F(-3.46f, 3.26f)));

    //
    // boundary
    v8_int8_t ret_val = point_in_polygon(poly, vector2F(2.836f, -3.424));
    EXPECT_TRUE((ret_val == 0) || (ret_val == 1));
}
*/