#include <iostream>
#include <gtest/gtest.h>

extern const float C_Epsilon = 1.0e-3f;

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
