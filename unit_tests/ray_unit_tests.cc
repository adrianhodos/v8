#include <gtest/gtest.h>

#include <v8/math/vector3.hpp>
#include <v8/math/objects/ray3.hpp>
#include <v8/math/distance/distance_point_ray.hpp>

extern const float C_Epsilon;

using namespace v8::math;

class ray_tests : public ::testing::Test {
protected :
    ray3F       ray_;
    vector3F    p0_;
    vector3F    p1_;

    void SetUp() {
        p0_ = vector3F(9.0f, -4.0f, 3.0f);
        p1_ = vector3F(9.0f, -4.0f, 4.0f);
        ray_ = ray3F::make_from_points(p0_, p1_);
    }
};

TEST_F(ray_tests, initialization) {
    const float dir_vec_expected[] = {
        0.0f, 0.0f, 1.0f
    };

    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(dir_vec_expected[i], ray_.direction_.elements_[i],
                    C_Epsilon);
    }

    const vector3F exp_origin(9.0f, -4.0f, 3.0f);

    EXPECT_TRUE(exp_origin == ray_.origin_);
}

TEST_F(ray_tests, distance_point) {
    const vector3F p_outside(-10.0f, 1.0f, 0.0f);

    float t_closest;
    float dst_squared = squared_distance(p_outside, ray_, &t_closest);

    EXPECT_NEAR(-3.0f, t_closest, C_Epsilon);
    EXPECT_NEAR(395.0f, dst_squared, C_Epsilon);

    const vector3F p_inside(10.0f, 2.0f, 5.0f);
    dst_squared = squared_distance(p_inside, ray_, &t_closest);
    EXPECT_NEAR(2.0f, t_closest, C_Epsilon);
    EXPECT_NEAR(37.0f, dst_squared, C_Epsilon);
}
