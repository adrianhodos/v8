#include <gtest/gtest.h>

#include "v8/math/color.hpp"

TEST(gfx_color_rgb_tests, conversion_from_rgba) {
    v8::math::color_rgb color_rgb(v8::math::color_rgb::from_u32_rgba(0xFF2D57FFU));

    EXPECT_NEAR(1.0f, color_rgb.Red, 1.0e-3);
    EXPECT_NEAR(0.176f, color_rgb.Green, 1.0e-3);
    EXPECT_NEAR(0.341f, color_rgb.Blue, 1.0e-3);
    EXPECT_NEAR(1.0f, color_rgb.Alpha, 1.0e-3);
}

TEST(gfx_color_rgb_tests, conversion_from_bgra) {
    v8::math::color_rgb color_rgb(v8::math::color_rgb::from_u32_bgra(0x572DFFFF));

    EXPECT_NEAR(1.0f, color_rgb.Red, 1.0e-3);
    EXPECT_NEAR(0.176f, color_rgb.Green, 1.0e-3);
    EXPECT_NEAR(0.341f, color_rgb.Blue, 1.0e-3);
    EXPECT_NEAR(1.0f, color_rgb.Alpha, 1.0e-3);
}

TEST(gfx_color_rgb_tests, conversion_from_argb) {
    v8::math::color_rgb color_rgb(v8::math::color_rgb::from_u32_argb(0xFFFF2D57));

    EXPECT_NEAR(1.0f, color_rgb.Red, 1.0e-3);
    EXPECT_NEAR(0.176f, color_rgb.Green, 1.0e-3);
    EXPECT_NEAR(0.341f, color_rgb.Blue, 1.0e-3);
    EXPECT_NEAR(1.0f, color_rgb.Alpha, 1.0e-3);
}

TEST(gfx_color_rgb_tests, conversion_to_uint32_rgba) {
    v8::math::color_rgb color_rgb(1.0f, 0.5f, 0.25f, 1.0f);

    EXPECT_EQ(0xFF8040FF, color_rgb.to_uint32_rgba());
}
