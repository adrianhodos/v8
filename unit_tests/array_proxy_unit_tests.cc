#include <gtest/gtest.h>
#include <v8/base/array_proxy.hpp>
#include <v8/base/count_of.hpp>

using namespace v8::base;

namespace {

struct Base {
    int val;

    Base(int i) : val(i) {}
};

struct Derived : public Base {
    Derived(int i) : Base(i) {}
};

} // anonymous namespace
TEST(array_proxy_tests, constructors_same_type) {
    int an_array[] = { 0, 1, 2, 3, 4, 5 };

    array_proxy<int> p0(an_array, an_array + dimension_of(an_array));
    array_proxy<int> p1(an_array, dimension_of(an_array));
    array_proxy<int> p2(&an_array[0], &an_array[dimension_of(an_array)]);

    EXPECT_EQ(dimension_of(an_array), p0.length());
    EXPECT_EQ(dimension_of(an_array), p1.length());
    EXPECT_EQ(dimension_of(an_array), p2.length());

    for (size_t i = 0; i < dimension_of(an_array); ++i) {
        EXPECT_EQ(an_array[i], p0[i]);
        EXPECT_EQ(an_array[i], *(p0.base() + i));
    }

    EXPECT_EQ(&an_array[0], p0.begin());
    EXPECT_EQ(&an_array[dimension_of(an_array)], p0.end());
}

TEST(array_proxy_tests, constructors_different_types) {
    Derived base_array[] = { Derived(0), Derived(1), Derived(2) };

    array_proxy<Base> p0(base_array, base_array + dimension_of(base_array));
    array_proxy<Base> p1(base_array, dimension_of(base_array));
    array_proxy<Base> p2(&base_array[0], &base_array[dimension_of(base_array)]);

    const size_t kDim = dimension_of(base_array);
    EXPECT_EQ(kDim, p0.length());
    EXPECT_EQ(kDim, p1.length());
    EXPECT_EQ(kDim, p2.length());

    for (size_t i = 0; i < p0.length(); ++i) {
        EXPECT_EQ(base_array[i].val, p0[i].val);
        EXPECT_EQ(base_array[i].val, (p0.base() + i)->val);
    }

    EXPECT_EQ(&base_array[0], p0.begin());
    EXPECT_EQ(&base_array[kDim], p0.end());
}

