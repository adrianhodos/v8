#include <gtest/gtest.h>

#include "v8/base/type_punning.hpp"

TEST(type_punning_tests, all) {
    v8::base::type_punning<int*, int> tp((int*) 0x12345);

    EXPECT_EQ(0x12345, tp.out_value);

    EXPECT_EQ(0x12345, v8::base::punning_cast<int>((int*) 0x12345));

    const float flt_val = 1.245f;
    v8_uint32_t val = v8::base::punning_cast<v8_uint32_t>(flt_val);

    EXPECT_EQ(flt_val, v8::base::punning_cast<float>(val));
}
