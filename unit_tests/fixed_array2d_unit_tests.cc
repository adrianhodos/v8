#include <algorithm>
#include <exception>
#include <string>
#include <gtest/gtest.h>
#include <v8/math/fixed_array_2d.hpp>

class oob_exception : public std::exception {
private :
    std::string description_;
public :
    oob_exception(const char* desc) : description_(desc ? desc : "No info") {}

    const char* what() const NOEXCEPT {
        return description_.c_str();
    }
};

struct oob_throw_policy {
    static size_t at(size_t idx, size_t max) {
        if (idx >= max)
            throw oob_exception("Out of bounds array access");

        return idx;
    }
};

using namespace v8::math;
using namespace std;

class FixedArray2DUnitTests : public ::testing::Test {
protected :
    fixed_array_2D<int, 4, 4> fix_arr_;

    void SetUp() {
        int seq_start = 0;
        generate_n(fix_arr_.begin(), fix_arr_.size(),
                   [&seq_start]() { return seq_start++; });
    }
};

TEST_F(FixedArray2DUnitTests, iterators) {
    int seq_start = 0;
    for (size_t y = 0; y < fix_arr_.dimension_y(); ++y) {
        for (size_t x = 0; x < fix_arr_.dimension_x(); ++x) {
            EXPECT_EQ(seq_start, fix_arr_(x, y));
            ++seq_start;
        }
    }
}

TEST_F(FixedArray2DUnitTests, clamping_policy) {
    fixed_array_2D<int, 4, 4, oob_clamp_policy> clamp_arr;
    copy(begin(fix_arr_), end(fix_arr_), begin(clamp_arr));

    EXPECT_EQ(clamp_arr(3, 3), clamp_arr(4, 4));
    EXPECT_EQ(clamp_arr(3, 3), clamp_arr(5, 4));
}

TEST_F(FixedArray2DUnitTests, wrap_policy) {
    fixed_array_2D<int, 4, 4, oob_wrap_policy> wrap_arr;
    copy(begin(fix_arr_), end(fix_arr_), begin(wrap_arr));

    EXPECT_EQ(wrap_arr(2, 2), wrap_arr(5, 5));
    EXPECT_EQ(wrap_arr(0, 0), wrap_arr(6, 6));
    EXPECT_EQ(wrap_arr(0, 0), wrap_arr(3, 3));
    EXPECT_EQ(wrap_arr(2, 1), wrap_arr(8, 7));
}

TEST_F(FixedArray2DUnitTests, throw_policy) {
    fixed_array_2D<int, 4, 4, oob_throw_policy> throw_arr;
    copy(begin(fix_arr_), end(fix_arr_), begin(throw_arr));

    EXPECT_NO_THROW(throw_arr(1, 2));
    EXPECT_THROW(throw_arr(4, 4), oob_exception);
}
