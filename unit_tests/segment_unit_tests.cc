#include <gtest/gtest.h>
#include <v8/math/vector3.hpp>
#include <v8/math/distance/distance_point_segment.hpp>
#include <v8/math/objects/segment3.hpp>

extern const float C_Epsilon;

using namespace v8::math;

class segment_tests : public ::testing::Test {
protected :
    segment3F   seg_;

    void SetUp() {
        seg_ = segment3F(vector3F(5.0f, -5.0f, 10.0f), vector3F::unit_z, 4.0f);
    }
};

TEST_F(segment_tests, init_from_points) {
    const vector3F e0(5.0f, 9.0f, 12.0f);
    const vector3F e1(15.0f, 6.0f, 4.0f);

    seg_ = segment3F(e0, e1);

    const float exp_center[] = {
        10.0f, 7.5f, 8.0f
    };
    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(exp_center[i], seg_.centre_.elements_[i], C_Epsilon);
    }

    const float exp_direction[] = {
        0.7602f, -0.2280f, -0.6082f
    };
    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(exp_direction[i], seg_.direction_.elements_[i], C_Epsilon);
    }

    EXPECT_NEAR(6.5764f, seg_.extent_, C_Epsilon);
}

TEST_F(segment_tests, end_points) {
    const vector3F exp_start_point(5.0f, -5.0f, 6.0f);
    const vector3F exp_end_point(5.0f, -5.0f, 14.0f);

    const vector3F actual_sp = seg_.get_start_point();
    const vector3F actual_ep = seg_.get_end_point();

    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(exp_start_point.elements_[i],
                    actual_sp.elements_[i], C_Epsilon);
    }

    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(exp_end_point.elements_[i],
                    actual_ep.elements_[i], C_Epsilon);
    }
}

TEST_F(segment_tests, distance_point_to_segment) {
    const vector3F Q0(4.0f, -10.0f, 0.0f);

    float t_value;
    float dst_squared = squared_distance(Q0, seg_, &t_value);

    EXPECT_NEAR(-10.0f, t_value, C_Epsilon);
    EXPECT_NEAR(62.0f, dst_squared, C_Epsilon);

    const vector3F Q1(8.0f, 2.0f, 12.0f);
    dst_squared = squared_distance(Q1, seg_, &t_value);

    EXPECT_NEAR(2.0f, t_value, C_Epsilon);
    EXPECT_NEAR(58.0f, dst_squared, C_Epsilon);

    const vector3F Q2(8.0f, -2.0f, 20.0f);
    dst_squared = squared_distance(Q2, seg_, &t_value);

    EXPECT_NEAR(10.0f, t_value, C_Epsilon);
    EXPECT_NEAR(54.0f, dst_squared, C_Epsilon);
}
