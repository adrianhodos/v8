#include <gtest/gtest.h>
#include <v8/math/vector3.hpp>
#include <v8/math/objects/plane.hpp>
#include <v8/math/distance/distance_point_plane.hpp>

extern const float C_Epsilon;
using namespace v8::math;
using namespace std;

class plane_tests : public ::testing::Test {
protected :
    plane3F P_;

    void SetUp() {
        P_ = plane3F(vector3F(-0.2591f, 0.4319f, 0.8638f),
                     vector3F(1.0f, -10.0f, 2.0f));
    }
};

TEST_F(plane_tests, init_from_point_and_normal) {
    EXPECT_NEAR(P_.offset_, 2.8507f, C_Epsilon);
}

TEST_F(plane_tests, init_from_points) {
    const vector3F p0(0.0f, 5.0f, 4.0f);
    const vector3F p1(9.0f, -3.0f, 6.0f);
    const vector3F p2(4.0f, 1.0f, -3.0f);

    P_ = plane3F::make_from_points(p0, p1, p2);

    const float expected_values[] = {
        0.6689f, 0.7421f, -0.0418f
    };
    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(expected_values[i], P_.normal_.elements_[i], C_Epsilon);
    }
    EXPECT_NEAR(-3.5433f, P_.offset_, C_Epsilon);
}

TEST_F(plane_tests, init_from_vectors_and_point) {
    const vector3F v0(-5.0f, 4.0f, 2.0f);
    const vector3F v1(9.0f, 1.0f, 0.0f);
    const vector3F p0(0.0f, 5.0f, 3.0f);

    P_ = plane3F::make_from_vectors_and_point(v0, v1, p0);

    const float expected_values[] = {
        -0.0446f, 0.4015f, -0.9147f
    };

    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(expected_values[i], P_.normal_.elements_[i], C_Epsilon);
    }
    EXPECT_NEAR(0.7362f, P_.offset_, C_Epsilon);
}

TEST_F(plane_tests, normalize_plane) {
    P_ = plane3F(vector3F(-3.0f, 5.0f, 10.0f), vector3F(12.0f, 9.45f, -16.22f));
    P_.normalize();

    const float expected_values[] = {
        -0.2591f, 0.4319f, 0.8638f
    };
    for (size_t i = 0; i < 3U; ++i) {
        EXPECT_NEAR(expected_values[i], P_.normal_.elements_[i], C_Epsilon);
    }
    EXPECT_NEAR(13.0400f, P_.offset_, C_Epsilon);
}

TEST_F(plane_tests, distance_point) {
    const vector3F p0(25.0f, -12.0f, 36.0f);
    const float dst = distance_signed(p0, P_);
    EXPECT_NEAR(22.287f, dst, C_Epsilon);
}
